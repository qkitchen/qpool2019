**Topic**: Basics of quantum optimization

**Contributors**: Adam Glos, Ayşin Taşdelen, Zoltán Zimborás

**Software** Qiskit, Ocean software

**Requirements**:
*  basics of quantum computing,
*  python (basic),
*  linear algebra

**Description**: The aim of the project is to provide basics of Ising optimization using NISQ-era algorithms. The project includes QUBO and Ising representation of combinatorial problem, quantum annealing and QAOA algorithms.


