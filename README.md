The aim of QPool2019 is to collaboratively prepare educational materials for quantum semester (http://qsoftware.lu.lv/index.php/qsemester/) and for possible summer/winter schools on quantum software and quantum technologies.

Depending on the success of this pool, we may repeat one or more pools in each year.

We use CC-BY or a similar license.

# Filling the pool

This is the main phase of the pool. It is expected to take between 2 and 4 months.

We list possible topics below. 

The participants can contribute based on their interests and preferences.

The levels of materials are expected to be very basic, basic, and elementary for QPool2019. 

We use Jupyter notebooks for preparing materials. 

Notebooks are expected to have certain tasks for the users to stimulate the learning process (_learning by doing_).

Possible topics:

_Please check the other participants of your topic(s). We highly encourage each participant to coordinate with the other participants on the same topic._

| **Topic(s)**                                                                                                                   | **Contributor(s)**   |
|--------------------------------------------------------------------------------------------------------------------------------|----------------------|
| Complex numbers, Bloch sphere, basic operations on Bloch sphere                                                                | _Abuzer Yakaryilmaz_ |
| Complex numbers                                                                                                                | _Maksims Dimitrijevs_|
| QFT, Shor's algorithm                                                                                                          | _Özlem Salehi Köken_ |
| Basics of quantum error correction                                                                                             |                      |
| Basics of quantum machine learning techniques and related classical literature                                                 | _Ayşin Taşdelen_     |
| Basics of quantum annealing, D-Wave, their software solutions                                                                  | _Ayşin Taşdelen_     |
| Introduction to or review of other quantum programming frameworks, e.g., <br> Xanadu products: https://www.xanadu.ai/software/ |                      |
| Basics of quantum games                                                                                                        |                      |
| Basics of quantum random walks                                                                                                 | _Oskar Słowik_       |
| Basics of quantum cryptography                                                                                                 | _Anastasija Trizna_  |
| Introduction to quantum internet                                                                                               |                      |
| Quantum compilers                                                                                                              | _Oskar Słowik_       |
| QAOA algorithm                                                                                                                 | _Adam Glos_          |
| Our own basic materials for some of the topics listed here (e.g., QAOA and VQE):<br> https://cs269q.stanford.edu/syllabus.html |                      |


# Specialization.

Once we have enough materials in the pool, they will be classified and then each class is continued as a separate project.

# The protocol for the pool
[Version 1.0 - September 4, 2019]

The current maintainers: **Adam Glos (aglos [at] iitis.pl)** and Abuzer Yakaryilmaz (abuzer [at] lu.lv)

Each candidate informs the maintaniers about her possible topic(s) that she will work on. Such information is kept in README files of the topics. Collaboration between institues are welcome!

Each participant joins the pool by her prepared notebook. (i.e., One should prepare at least one notebook to get a role in QPool2019.)

Each notebook should have at least one author. The author(s) has (have) the rights on the document. Specifically, they have the right to add new author(s) or contributor(s).

Participants get higher roles based on their contributions.

The logo of QWorld should be on each document. Each author may add her own group's logo to her document(s).

Participants use a Slack channel for communication. 

As long as it is possible, python is used as the main programming language.

The participants may follow the format of the example directory in the pool. (See also https://gitlab.com/qkitchen/basics-of-quantum-computing)

The participants are free to use any (quantum) library for their programs. The experience in different quantum libraries is desirable.

**NEW** For each topic a separate branch is created. The participants have full permissions to the branch. Whenever they believe the materials are good enough to be "published", please make a merge request to the *main* branch.
Optionally, pull requests are also possible.
