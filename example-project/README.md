**Topic**: Exemplary project

**Contributors**: Adam Glos, Abuzer Yakaryilmaz

**Requirements**:
*  basics of quantum computing
*  python (basic)

**Description**: The aim of the project is to present an exemplary course. *Note*: we encourage to use markdown files for README, however .txt files are also acceptable.
